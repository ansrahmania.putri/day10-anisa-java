package com.codeflex.springboot.service;


import com.codeflex.springboot.model.Product;

import java.util.List;

public interface ProductServiceDB {

    Product findById(long id);

    Product findByName(String name);

    void saveProduct(Product product);

    void updateProduct(long id, Product product);

    void deleteProductById(long id);

    List<Product> findAllProducts();

    void deleteAllProducts();

    boolean isProductExist(Product product);

}
